CREATE SCHEMA IF NOT EXISTS mydb;

CREATE TABLE IF NOT EXISTS mydb.address (
  id_address SERIAL PRIMARY KEY NOT NULL,
  city VARCHAR(45) NOT NULL,
  zip_code INTEGER NOT NULL,
  street VARCHAR(45),
  house_number INTEGER NOT NULL);
  
  CREATE TABLE IF NOT EXISTS mydb.Person (
  id_person SERIAL PRIMARY KEY NOT NULL,
  surname VARCHAR(45) NOT NULL,
  given_name VARCHAR(45) NOT NULL,
  age INTEGER NOT NULL,
  sex CHAR(1) NOT NULL);
  
  CREATE TABLE IF NOT EXISTS mydb.contact (
  phone_number VARCHAR(45) NOT NULL,
  email VARCHAR(45),
  id_person INTEGER NOT NULL,
  FOREIGN KEY (id_person) references mydb.person);
  
  CREATE TABLE IF NOT EXISTS mydb.Job (
  id_job SERIAL PRIMARY KEY NOT NULL,
  company VARCHAR(45) NOT NULL,
  salary INTEGER NOT NULL);
	  
  CREATE TABLE IF NOT EXISTS mydb.Person_has_Address (
  id_person INTEGER NOT NULL,
  id_address INTEGER NOT NULL,
  FOREIGN KEY (id_person) references mydb.person,
  FOREIGN KEY (id_address) references mydb.address);
    
  CREATE TABLE IF NOT EXISTS mydb.role_type (
  id_role SERIAL PRIMARY KEY NOT NULL,
  role VARCHAR(45) NOT NULL);
  
  CREATE TABLE IF NOT EXISTS mydb.Person_has_job (
  id_person INTEGER NOT NULL,
  id_job INTEGER NOT NULL,
  id_role INTEGER NOT NULL,
  FOREIGN KEY (id_person) references mydb.person,
  FOREIGN KEY (id_job) references mydb.job,
  FOREIGN KEY (id_role) references mydb.role_type);
  
  CREATE TABLE IF NOT EXISTS mydb.relationship_type (
  id_relationship_type SERIAL PRIMARY KEY NOT NULL,
  relationship_type VARCHAR(45) NOT NULL);

  CREATE TABLE IF NOT EXISTS mydb.status_type (
  id_status_type SERIAL PRIMARY KEY NOT NULL,
  status_type VARCHAR(45) NOT NULL);
  
  CREATE TABLE IF NOT EXISTS mydb.status (
  id_person INTEGER NOT NULL,
  id_Status_type INTEGER NOT NULL,
  FOREIGN KEY (id_person) references mydb.person,
  FOREIGN KEY (id_status_type) references mydb.status_type);
  
  CREATE TABLE IF NOT EXISTS mydb.testing (
  testing_time timestamp NOT NULL,
  id_address INTEGER NOT NULL,
  id_person INTEGER NOT NULL,
  FOREIGN KEY (id_address) references mydb.address,
  FOREIGN KEY (id_person) references mydb.person);
  
  CREATE TABLE IF NOT EXISTS mydb.person_has_relationship (
  id_relationship INT NOT NULL,
  id_person1 INT NOT NULL,
  id_person2 INT NOT NULL,
  FOREIGN KEY (id_relationship) references mydb.relationship_type,
  FOREIGN KEY (id_person1) references mydb.person,
  FOREIGN KEY (id_person2) references mydb.person);